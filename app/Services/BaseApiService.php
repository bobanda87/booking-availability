<?php

namespace App\Services;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

/**
 * Class BaseApiService
 *
 * @package App\Services
 */
abstract class BaseApiService
{
    const HTTP_GET = 'get';
    const HTTP_PATCH = 'patch';
    const HTTP_POST = 'post';
    const HTTP_PUT = 'put';

    const SUPPORTED_METHODS = [
        self::HTTP_GET,
        self::HTTP_PATCH,
        self::HTTP_POST,
        self::HTTP_PUT,
    ];

    /**
     * @param string $method
     * @param string $uri
     * @param array $payload
     *
     * @return Response|null
     */
    protected static function executeRequest(string $method, string $uri, array $payload = []) : ?Response
    {
        if (! in_array($method, self::SUPPORTED_METHODS)) {
            throw new \InvalidArgumentException('Unsupported HTTP method ' . $method);
        }

        $response = null;

        try {
            /* @var Response $response*/
            $response = Http::{$method}($uri, $payload);
        } catch (\Exception $e) {
            // Gracefully handle exception
            // Log issue into metrics for tracking
        } finally {
            return $response;
        }
    }

    /**
     * @param string $uri
     * @param array  $payload
     *
     * @throws \InvalidArgumentException
     *
     * @return Response|null
     */
    protected static function executeGet(string $uri, array $payload = []) : ?Response
    {
        return self::executeRequest(self::HTTP_GET, $uri, $payload);
    }

}
