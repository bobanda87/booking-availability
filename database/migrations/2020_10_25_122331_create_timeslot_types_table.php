<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeslotTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timeslot_types', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->integer('slot_size');
            $table->boolean('public_bookable');
            $table->string('color');
            $table->string('icon');
            $table->foreignUuid('clinic_id')->constrained()->cascadeOnDelete();
            $table->string('deleted')->nullable();
            $table->boolean('out_of_office');
            $table->boolean('enabled');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeslot_types');
    }
}
