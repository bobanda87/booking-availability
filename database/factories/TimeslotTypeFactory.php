<?php

namespace Database\Factories;

use App\Models\DTO\TimeslotType;
use Illuminate\Database\Eloquent\Factories\Factory;

class TimeslotTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TimeslotType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
