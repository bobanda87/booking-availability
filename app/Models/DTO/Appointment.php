<?php

namespace App\Models\DTO;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Appointment
 *
 * @package App\Models\DTO
 */
class Appointment extends BaseDTO
{
    protected const ALLOWED_FIELDS = [
        'id' => Uuid::class,
        'name' => 'string',
    ];

    /**
     * Scope a query to include timeslots within time period from calendars for a specific time slot type
     *
     * @param Builder $query
     * @param $calendarIds
     * @param $dateFrom
     * @param $dateTo
     * @param $timeSlotType
     *
     * @return Builder
     */
    public function scopeFromCalendarInTimePeriod($query, array $calendarIds, $dateFrom, $dateTo, $timeSlotType)
    {
        return $query
            ->whereIn('calendar_id', $calendarIds)
            ->where('start', '>=', $dateFrom)
            ->where('end', '<=', $dateTo)
            ->where('type_id', $timeSlotType)
            ->orderBy('start', 'asc');
    }
}
