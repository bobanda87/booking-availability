<?php

namespace App\Models;

/**
 * Class Booking
 *
 * @package App\Models
 */
class Booking
{
    public const VALIDATION_FIELDS = [
        'periodToSearch' => 'required|string',
        'calendarIds' => 'required|array',
        'duration' => 'required|int',
        'timeSlotType' => 'string'
    ];

}
