<?php

namespace Tests\Unit;

use App\Services\BookingService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\TestCase;
use Tests\Fixtures\TimeslotsFixture;

/**
 * Class BookingServiceTest
 *
 * @package Tests\Unit
 */
class BookingServiceTest extends TestCase
{
    use RefreshDatabase;
    use TimeslotsFixture;

    protected function setUp(): void
    {
        parent::setUp();

        $this->initTimeslots();
    }

    /**
     * Testing merging timeslots for duration 30 minutes
     *
     * @return void
     */
    public function testMergeTimeslotsForDuration30()
    {
        $duration30 = BookingService::mergeTimeslotsForDuration($this->timeslots, 30);
        $this->assertEquals($duration30, $this->duration30);
    }

    /**
     * Testing merging timeslots for duration 60 minutes
     *
     * @return void
     */
    public function testMergeTimeslotsForDuration60()
    {
        $duration60 = BookingService::mergeTimeslotsForDuration($this->timeslots, 60);
        $this->assertEquals($duration60, $this->duration60);
    }

    /**
     * Testing merging timeslots for duration 90 minutes
     *
     * @return void
     */
    public function testMergeTimeslotsForDuration90()
    {
        $duration90 = BookingService::mergeTimeslotsForDuration($this->timeslots, 90);
        $this->assertEquals($duration90, $this->duration90);
    }
}
