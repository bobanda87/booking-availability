<?php

namespace Database\Seeders;

/**
 * Class TimeslotSeeder
 *
 * @package Database\Seeders
 */
class TimeslotSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::import('timeslots');
    }
}
