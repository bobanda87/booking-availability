<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

/**
 * Class BaseKeySeeder
 *
 * @package Database\Seeders
 */
class BaseKeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param string $tableName
     */
    public function import(string $tableName)
    {
        \DB::table($tableName)->delete();

        $json = Storage::get("data/$tableName.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            foreach ($object as $key => $prop) {
                // transform key to be a property of an object
                $keyObjects = array_merge([
                    'id' => $key,
                ], (array) $prop);

                // removing array keys - not needed for the task
                foreach ($keyObjects as $tmpKey => $keyObject) {
                    if (is_array($keyObject)) {
                        unset($keyObjects[$tmpKey]);
                    }
                }

                \DB::table($tableName)->insert((array) $keyObjects);
            }
        }
    }
}
