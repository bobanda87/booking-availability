<?php

namespace App\Services;

use App\Models\DTO\Appointment;
use App\Models\DTO\Timeslot;
use Carbon\Carbon;

/**
 * Class BookingService
 *
 * @package App\Services
 */
class BookingService
{
    /**
     * @param array $calendarIds
     * @param int $duration
     * @param string $dateFrom
     * @param string $dateTo
     * @param array $timeSlotType
     * @return array
     */
    public static function getAvailableTimeslots(array $calendarIds, int $duration, string $dateFrom, string $dateTo, $timeSlotType) : array
    {
        $timeslots = Timeslot::fromCalendarInTimePeriod($calendarIds, $dateFrom, $dateTo, $timeSlotType)->get();
        $appointments = Appointment::fromCalendarInTimePeriod($calendarIds, $dateFrom, $dateTo, $timeSlotType)->get();

        $availableTimeslots = BookingService::filterAppointmentsFromTimeslots($timeslots, $appointments);

        return BookingService::mergeTimeslotsForDuration($availableTimeslots, $duration);
    }

    /**
     * @param $timeslots
     * @param $appointments
     *
     * @return array
     */
    public static function filterAppointmentsFromTimeslots($timeslots, $appointments): array
    {
        $overlap = [];
        foreach ($timeslots as $timeslot) {
            foreach ($appointments as $appointment) {
                if ($appointment->start === $timeslot->start) {
                    $overlap[] = $timeslot->id;
                }
            }
        }

        return array_values(
            array_filter($timeslots->toArray(), function($timeslot) use ($overlap){
                return !in_array($timeslot['id'], $overlap);
            })
        );
    }

    /**
     * @param $timeslots
     * @param $duration
     *
     * @return array
     */
    public static function mergeTimeslotsForDuration($timeslots, $duration): array
    {
        $availableTimeslotsWithDuration = [];
        for ($i = 0; $i < count($timeslots); $i++) {
            $startTime = Carbon::parse($timeslots[$i]['start']);
            $endTime = Carbon::parse($timeslots[$i]['end']);
            $intervalDuration = $endTime->diff($startTime)->i;

            $j = $i + 1;
            while ($intervalDuration < $duration && $j < count($timeslots)) {
                if ($timeslots[$j]['start'] !== $timeslots[$j - 1]['end']) {
                    break;
                }

                $startTime = Carbon::parse($timeslots[$j]['start']);
                $endTime = Carbon::parse($timeslots[$j]['end']);
                $intervalDuration += $endTime->diff($startTime)->i;
                $j++;
            }

            if ($intervalDuration >= $duration) {
                $availableTimeslotInterval = $timeslots[$i];
                $availableTimeslotInterval['end'] = $timeslots[$j - 1]['end'];
                $availableTimeslotsWithDuration[] = $availableTimeslotInterval;
            }
        }

        return $availableTimeslotsWithDuration;
    }
}
