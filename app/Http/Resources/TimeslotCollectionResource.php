<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class TimeslotCollectionResource
 *
 * @package App\Http\Resources
 */
class TimeslotCollectionResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'calendar_id' => $this->calendar_id,
            'type_id' => $this->type_id,
            'start' => $this->start,
            'end' => $this->end
        ];
    }
}
