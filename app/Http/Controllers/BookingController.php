<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Services\BookingService;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class CalendarController
 *
 * @package App\Http\Controllers
 */
class BookingController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function findAvailableTime(Request $request): JsonResponse
    {
        $validator = \Validator::make($request->all(), Booking::VALIDATION_FIELDS);

        if ($validator->fails()) {
            return $this->failedJsonResponse($validator->errors());
        }

        $dateInterval = explode('/', $request->get('periodToSearch'));
        try {
            $dateFrom = Carbon::parse($dateInterval[0])->toDateTimeString();
            $dateTo = Carbon::parse($dateInterval[1])->toDateTimeString();
        } catch (InvalidFormatException $exception) {
            return $this->failedJsonResponse($exception->getMessage());
        }

        $availableTimeslots = BookingService::getAvailableTimeslots(
            $request->get('calendarIds'),
            (int) $request->get('duration'),
            $dateFrom,
            $dateTo,
            $request->get('timeSlotType')
        );

        return $this->successJsonResponse($availableTimeslots);
    }

}
