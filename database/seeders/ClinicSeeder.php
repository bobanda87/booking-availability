<?php

namespace Database\Seeders;

/**
 * Class ClinicSeeder
 *
 * @package Database\Seeders
 */
class ClinicSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::import('clinics');
    }
}
