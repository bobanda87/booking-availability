<?php

namespace App\Models\DTO;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Support\Facades\Date;

/**
 * Class Patient
 *
 * @package App\Models\DTO
 */
class Patient extends BaseDTO
{
    protected const ALLOWED_FIELDS = [
        'id' => Uuid::class,
        'firstname' => 'string',
        'middlename' => 'string',
        'lastname' => 'string',
        'personal_id' => 'string',
        'birth_date' => 'date',
        'gender' => 'string'
    ];
}
