<?php

namespace App\Models\DTO;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

/**
 * Class Clinic
 *
 * @package App\Models\DTO
 */
class Clinic extends BaseDTO
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'name'
    ];

    protected const ALLOWED_FIELDS = [
        'id' => Uuid::class,
        'name' => 'string',
    ];

}
