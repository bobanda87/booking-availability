<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('patient_id')->constrained()->cascadeOnDelete();
            $table->foreignUuid('calendar_id')->constrained()->cascadeOnDelete();
            $table->dateTime('start');
            $table->dateTime('end');
            $table->string('patient_comment')->nullable();
            $table->string('note')->nullable();
            $table->uuid('time_slot_type_id');
            $table->foreign('time_slot_type_id')->references('id')->on('timeslot_types')->cascadeOnDelete();
            $table->uuid('type_id')->nullable();
            $table->integer('state');
            $table->string('out_of_office_location')->default('');
            $table->boolean('out_of_office');
            $table->boolean('completed');
            $table->boolean('is_scheduled');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
