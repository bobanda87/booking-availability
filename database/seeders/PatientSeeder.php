<?php

namespace Database\Seeders;

/**
 * Class PatientSeeder
 *
 * @package Database\Seeders
 */
class PatientSeeder extends BaseKeySeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::import('patients');
    }
}
