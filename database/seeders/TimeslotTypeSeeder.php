<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

/**
 * Class TimeslotTypeSeeder
 *
 * @package Database\Seeders
 */
class TimeslotTypeSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::import('timeslot_types');
    }
}
