<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Trait JsonResponseTrait
 */
trait JsonResponseTrait
{
    /**
     * @param $error
     *
     * @return JsonResponse
     */
    protected function failedJsonResponse($error): JsonResponse
    {
        return response()->json([
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => $error
        ]);
    }

    /**
     * @param $data
     *
     * @return JsonResponse
     */
    protected function successJsonResponse($data): JsonResponse
    {
        return response()->json([
            'status' => Response::HTTP_OK,
            'data' => $data
        ]);
    }
}
