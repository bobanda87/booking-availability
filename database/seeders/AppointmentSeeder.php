<?php

namespace Database\Seeders;

/**
 * Class AppointmentSeeder
 *
 * @package Database\Seeders
 */
class AppointmentSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::import('appointments');
    }
}
