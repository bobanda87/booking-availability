<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TimeslotResource
 *
 * @package App\Http\Resources
 */
class TimeslotResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'calendar_id' => $this->calendar_id,
            'type_id' => $this->type_id,
            'start' => $this->start,
            'end' => $this->end
        ];
    }
}
