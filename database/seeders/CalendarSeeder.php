<?php

namespace Database\Seeders;

/**
 * Class CalendarSeeder
 *
 * @package Database\Seeders
 */
class CalendarSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::import('calendars');
    }
}
