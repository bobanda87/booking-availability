<?php

namespace Database\Factories;

use App\Models\DTO\PatientContactAddress;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientContactAddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PatientContactAddress::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
