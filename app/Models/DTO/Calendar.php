<?php

namespace App\Models\DTO;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

/**
 * Class Calendar
 *
 * @package App\Models\DTO
 */
class Calendar extends BaseDTO
{
    protected const ALLOWED_FIELDS = [
        'id' => Uuid::class,
        'name' => 'string',
    ];
}
