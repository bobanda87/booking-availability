### Running locally

1. Create database `patientsky`
1. Copy `.env.example` into `.env` file
1. Configure database connection in `.env` file
1. Install dependencies using composer `composer install`
1. Run migrations `php artisan migrate`
1. Populate database data `php artisan db:seed`
1. Run unit tests `php artisan test`
1. Run application `php artisan serve`
1. Run curl commands to fetch available timeslots

### Curl commands samples

Fetch intervals for 30 minutes for a single calendar.
```
curl --location --request GET 'http://127.0.0.1:8000/available-time' \
--header 'Content-Type: application/json' \
--data-raw '{
    "periodToSearch": "2019-03-01T13:00:00Z/2019-05-11T15:30:00Z",
    "duration": "30",
    "calendarIds": [
        "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9"
    ],
    "timeSlotType": "4529821e-975e-11e5-bbaf-c8e0eb18c1e9"
}'
```

Fetch intervals for 60 minutes for a single calendar.
```
curl --location --request GET 'http://127.0.0.1:8000/available-time' \
--header 'Content-Type: application/json' \
--data-raw '{
    "periodToSearch": "2019-03-01T13:00:00Z/2019-05-11T15:30:00Z",
    "duration": "36",
    "calendarIds": [
        "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9"
    ],
    "timeSlotType": "4529821e-975e-11e5-bbaf-c8e0eb18c1e9"
}'
```

Fetch intervals for 30 minutes for multiple calendars.
```
curl --location --request GET 'http://127.0.0.1:8000/available-time' \
--header 'Content-Type: application/json' \
--data-raw '{
    "periodToSearch": "2019-03-01T13:00:00Z/2019-05-11T15:30:00Z",
    "duration": 30,
    "calendarIds": [
        "452dccfc-975e-11e5-bfa5-c8e0eb18c1e9", 
        "48644c7a-975e-11e5-a090-c8e0eb18c1e9",
        "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9"
    ],
    "timeSlotType": "4529821e-975e-11e5-bbaf-c8e0eb18c1e9"
}'
```

