<?php

namespace App\Models\DTO;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class Timeslot
 *
 * @package App\Models\DTO
 */
class Timeslot extends BaseDTO
{
    /**
     * Scope a query to include timeslots within time period from calendars for a specific time slot type
     *
     * @param Builder $query
     * @param $calendarIds
     * @param $dateFrom
     * @param $dateTo
     * @param $timeSlotType
     *
     * @return Builder
     */
    public function scopeFromCalendarInTimePeriod($query, array $calendarIds, $dateFrom, $dateTo, $timeSlotType)
    {
        return $query
            ->whereIn('calendar_id', $calendarIds)
            ->where('start', '>=', $dateFrom)
            ->where('end', '<=', $dateTo)
            ->where('type_id', $timeSlotType)
            ->orderBy('start', 'asc');
    }
}
