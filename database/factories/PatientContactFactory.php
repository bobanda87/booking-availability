<?php

namespace Database\Factories;

use App\Models\DTO\PatientContact;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PatientContact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
