<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 *
 * @package Database\Seeders
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ClinicSeeder::class);
        $this->call(TimeslotTypeSeeder::class);
        $this->call(CalendarSeeder::class);
        $this->call(TimeslotSeeder::class);
        $this->call(PatientSeeder::class);
        $this->call(AppointmentSeeder::class);
    }
}
