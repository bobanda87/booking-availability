<?php

namespace Tests\Fixtures;

/**
 * Trait TimeslotsFixture
 *
 * @package Tests\Unit\Traits
 */
trait TimeslotsFixture
{
    protected
        $timeslots,
        $duration30,
        $duration60,
        $duration90;

    /**
     * Initialize timeslots and available timeslots for durations
     */
    protected function initTimeslots(): void
    {
        $this->initDuration30();
        $this->initDuration60();
        $this->initDuration90();
    }

    /**
     * Inititalizing timeslots for duration 30 minutes
     */
    public function initDuration30()
    {
        $this->timeslots = [
            [
                "id" => "af87de36-3690-4caa-a100-4d28b05e3d94",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 08:00:00",
                "end" => "2019-04-23 08:30:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "4955e65a-3b3b-475f-94c4-21bd5a110d48",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 08:30:00",
                "end" => "2019-04-23 09:00:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "957a1cf7-8b95-432f-93dd-3c3ca55c9e50",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 09:00:00",
                "end" => "2019-04-23 09:30:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "518654f4-bd4e-485f-a04e-4881268195a0",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 09:30:00",
                "end" => "2019-04-23 10:00:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "6747a2b5-5eea-4fd6-ac85-0833011a3d90",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 10:00:00",
                "end" => "2019-04-23 10:30:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "75c3d0d9-d9b0-4c21-81a4-73c5c136391f",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 13:00:00",
                "end" => "2019-04-23 13:30:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "3ba8788d-ab01-41d5-bee7-0974d7f56180",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 13:30:00",
                "end" => "2019-04-23 14:00:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "1a6edf28-770d-43f5-9498-907f6f79ee57",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 14:00:00",
                "end" => "2019-04-23 14:30:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ]
        ];

        $this->duration30 = $this->timeslots;
    }

    /**
     * Inititalizing timeslots for duration 60 minutes
     */
    public function initDuration60()
    {
        $this->duration60 = [
            [
                "id" => "af87de36-3690-4caa-a100-4d28b05e3d94",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 08:00:00",
                "end" => "2019-04-23 09:00:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "4955e65a-3b3b-475f-94c4-21bd5a110d48",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 08:30:00",
                "end" => "2019-04-23 09:30:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "957a1cf7-8b95-432f-93dd-3c3ca55c9e50",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 09:00:00",
                "end" => "2019-04-23 10:00:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "518654f4-bd4e-485f-a04e-4881268195a0",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 09:30:00",
                "end" => "2019-04-23 10:30:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "75c3d0d9-d9b0-4c21-81a4-73c5c136391f",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 13:00:00",
                "end" => "2019-04-23 14:00:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "3ba8788d-ab01-41d5-bee7-0974d7f56180",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 13:30:00",
                "end" => "2019-04-23 14:30:00",
                "public_bookable" => 1,
                "out_of_office" => 0,
            ]
        ];
    }

    /**
     * Inititalizing timeslots for duration 90 minutes
     */
    public function initDuration90()
    {
        $this->duration90 = [
            [
                "id" => "af87de36-3690-4caa-a100-4d28b05e3d94",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 08:00:00",
                "end" => "2019-04-23 09:30:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "4955e65a-3b3b-475f-94c4-21bd5a110d48",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 08:30:00",
                "end" => "2019-04-23 10:00:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "957a1cf7-8b95-432f-93dd-3c3ca55c9e50",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 09:00:00",
                "end" => "2019-04-23 10:30:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ],
            [
                "id" => "75c3d0d9-d9b0-4c21-81a4-73c5c136391f",
                "calendar_id" => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
                "type_id" => "4529821e-975e-11e5-bbaf-c8e0eb18c1e9",
                "start" => "2019-04-23 13:00:00",
                "end" => "2019-04-23 14:30:00",
                "public_bookable" => 1,
                "out_of_office" => 0
            ]
        ];
    }
}
